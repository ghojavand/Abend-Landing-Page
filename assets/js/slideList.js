function reAnimate(sel, animateClass) {
  $(sel).removeClass(animateClass);
  setTimeout(function() {
    $(sel).addClass(animateClass);
  }, 1);
}

$('.list1 a').click(function() {
  reAnimate('.list1', 'slide-list');
});
